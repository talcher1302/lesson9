#pragma once

using namespace std;
template <class T>
class BSNode
{
public:
	BSNode(T data)
	{
		this->_data = data;
		this->_left = nullptr;
		this->_right = nullptr;
		this->_count = 1;
	}
	BSNode(const BSNode& other)
	{
		this->_data = other._data;
		this->_left = other._left;
		this->_right = other._right;
		this->_count = other._count;
	}

	~BSNode()
	{

	}

	void insert(T value)
	{
		if (value < this->_data) {  //If the value is smaller it goes to the left

			if (this->_left == nullptr) {
				this->_left = new BSNode<T>(value);
			}
			else
				return this->_left->insert(value);
		}
		else if (value > this->_data) { //If the value is bigger it goes to the right
			if (this->_right == nullptr) {
				this->_right = new BSNode<T>(value);
			}
			else
				return this->_right->insert(value);
		}
		else
			this->_count++;
	}
	BSNode& operator=(const BSNode& other)
	{
		this->_data = other._data;
		this->_left = other._left;
		this->_right = other._right;
		return *this;
	}

	bool isLeaf() const
	{
		return (this != nullptr && this->getLeft() == nullptr && this->getRight() == nullptr);
	}
	T getData() const
	{
		return this->_data;
	}
	BSNode* getLeft() const
	{
		return this._left;
	}
	BSNode* getRight() const
	{
		return this._right;
	}

	bool search(T val) const
	{
		if (this == NULL)
			return false;
		if (this->_data == val) //Value found
			return true;

		if (this->_data < val) //Searches to the left if the value is smaller, otherwise right
			return this->_right->search(val);
		return this->_left->search(val);
	}

	int getHeight() const
	{
		if (this == nullptr)
			return 0;
		int lh = this->_left->getHeight();
		int rh = this->_right->getHeight();
		if (lh > rh)
			return lh + 1;
		return rh + 1;
	}
	int getDepth(const BSNode& root) const
	{
		if (!root.search(this->_data))
			return -1;
		this->getCurrNodeDistFromInputNode(&root);
	}

	void printNodes() const //for question 1 part C
	{
		if (this != nullptr)
		{
			this->_left->printNodes();
			std::cout << "Value: " << this->_data << ", count: " << this->_count << endl;
			this->_right->printNodes();
		}
	}

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const
	{
		if (this == nullptr)
			return 0;
		if (node->getLeft() != nullptr) return this->getCurrNodeDistFromInputNode(node->getLeft()) + 1;
		if (node->getRight() != nullptr) return this->getCurrNodeDistFromInputNode(node->getRight()) + 1;
		return 0;
	}

};