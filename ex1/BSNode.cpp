#include "BSNode.h"
#include <string>
#include <math.h>
#include <algorithm>
#include <iostream>
using namespace std;

BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}
BSNode::BSNode(const BSNode& other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	this->_count = other._count;
}

BSNode::~BSNode()
{

}

//inserting a node to the BS tree
void BSNode::insert(string value)
{
	if (value.compare(this->_data) < 0) { //If the value is smaller it goes to the left

		if (this->_left == nullptr) {
			this->_left = new BSNode(value);
		}
		else
			return this->_left->insert(value);
	}
	else if (value.compare(this->_data) > 0) { //If the value is bigger it goes to the right
		if (this->_right == nullptr) {
			this->_right = new BSNode(value);
		}
		else
			return this->_right->insert(value);
	}
	else
		this->_count++;
}

BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	return *this;
}

bool BSNode::isLeaf() const
{
	return (this != nullptr && this->getLeft() == nullptr && this->getRight() == nullptr);
}

//getters
string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{
	if (this == nullptr)
		return false;
	if (this->_data == val) //Value found
		return true;

	if (this->_data.compare(val) < 0) //Searches to the left if the value is smaller, otherwise right
		return this->_right->search(val);
	return this->_left->search(val);
}

int BSNode::getHeight() const
{
	if (this == nullptr)
		return 0;
	int lh = this->_left->getHeight();
	int rh = this->_right->getHeight();
	if (lh > rh)
		return lh + 1;
	return rh + 1;
		
}

int BSNode::getDepth(const BSNode& root) const
{
	if (!root.search(this->_data))
		return -1;
	this->getCurrNodeDistFromInputNode(&root);
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (node == nullptr)
		return 0;
	if (node->getData() == this->getData())
		return 1;
	return 1 + std::max(this->getCurrNodeDistFromInputNode(node->getLeft()), this->getCurrNodeDistFromInputNode(node->getRight()));
}

void BSNode::printNodes() const
{
	if (this != nullptr)
	{
		this->_left->printNodes();
		std::cout << "Value: " << this->_data << ", count: " << this->_count << endl;
		this->_right->printNodes();
	}
}