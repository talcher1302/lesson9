#pragma once
#include <iostream>
class MyObject
{
public: int num;
	MyObject(int num);
	MyObject();

	~MyObject();

	bool operator==(const MyObject& other);
	void operator=(const MyObject& other);
	bool operator>(const MyObject& other);
	friend std::ostream& operator<<(std::ostream& os, const MyObject obj);
};
