#include "MyObject.h"
#include <iostream>

MyObject::MyObject(int num)
{
	this->num = num;
}

MyObject::MyObject()
{
	this->num = num;
}

MyObject::~MyObject()
{

}

//checks if equal
bool MyObject::operator==(const MyObject& other)
{
	return this->num == other.num;
}

//copy value
void MyObject::operator=(const MyObject& other)
{
	this->num = other.num;
}

//checks if value is bigger
bool MyObject::operator>(const MyObject& other)
{
	return (this->num > other.num);
}

std::ostream& operator<<(std::ostream& os, const MyObject obj)
{
	os << obj.num;
	return os;
}