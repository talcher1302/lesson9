#include "MyObject.h"

using namespace std;

#define EQUAL 0
#define BIGGER -1
#define SMALLER 1

//Function compares between 2 parameters
template <class T>
int compare(T par1, T par2)
{
	if (par1 == par2)
		return EQUAL;
	else if (par1 < par2)
		return SMALLER;
	else return BIGGER;
}

//Function sorts the array by bubble sort
template <class arrT>
void bubbleSort(arrT arr[], int n)
{
	int i, j;
	arrT temp;
	for (i = 0; i < n - 1; i++)
		for (j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
}

//Function prints an array
template <class printT>
void printArray(printT arr[], int size)
{
	for (int i = 0; i < size; i++)
		std::cout << arr[i] << endl;
}